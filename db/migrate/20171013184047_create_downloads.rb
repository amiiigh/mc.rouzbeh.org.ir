class CreateDownloads < ActiveRecord::Migration[5.1]
  def change
    create_table :downloads do |t|
      t.string :title
      t.text :desc
      t.string :file

      t.timestamps
    end
  end
end
