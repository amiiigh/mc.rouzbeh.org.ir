class RemoveCurrentYearFromConfigs < ActiveRecord::Migration[5.1]
  def change
    remove_column :configs, :current_year, :integer
  end
end
