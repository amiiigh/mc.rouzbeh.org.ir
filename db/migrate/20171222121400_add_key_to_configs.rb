class AddKeyToConfigs < ActiveRecord::Migration[5.1]
  def change
    add_column :configs, :key, :string
  end
end
