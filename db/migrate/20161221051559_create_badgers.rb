class CreateBadgers < ActiveRecord::Migration[5.0]
  def change
    create_table :badgers do |t|
      t.integer :student_id
      t.integer :badge_id

      t.timestamps
    end
  end
end
