class AddMoneyToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :money, :int
  end
end
