class CreateLessons < ActiveRecord::Migration[5.1]
  def change
    create_table :lessons do |t|
      t.string :title
      t.text :desc
      t.string :image
      t.string :file

      t.timestamps
    end
  end
end
