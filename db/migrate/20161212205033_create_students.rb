class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :fname
      t.string :lname
      t.string :class_name
      t.integer :class_no
      t.integer :points
      t.string :avatar

      t.timestamps
    end
  end
end
