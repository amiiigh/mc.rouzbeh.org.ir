class AddValueToConfigs < ActiveRecord::Migration[5.1]
  def change
    add_column :configs, :value, :string
  end
end
