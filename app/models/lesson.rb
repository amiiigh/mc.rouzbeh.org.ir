class Lesson < ApplicationRecord
	mount_uploader :image, LessonImageUploader
	mount_uploader :file , LessonFileUploader
end
