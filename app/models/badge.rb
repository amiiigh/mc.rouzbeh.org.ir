class Badge < ApplicationRecord
	has_many :badger
	has_many :students , :through => :badger
	mount_uploader :image, StudentAvatarUploader
end
