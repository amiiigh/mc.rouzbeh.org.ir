class Student < ApplicationRecord
	mount_uploader :avatar, StudentAvatarUploader
	has_one :user
	has_one :year
	has_many :badges , :through => :badger
	has_many :badger
  # after_initialize :init

  def get_cred
  	self.fname + ' ' + self.lname
  end


  def self.import(file)
  	Spreadsheet.client_encoding = 'UTF-8'
  	tmp = file.tempfile
  	book = Spreadsheet.open tmp.path
  	sheet1 = book.worksheet 0
  	header = sheet1.row(0)
  	sheet1.each 1 do |row|
  		class_number = row[0]
  		class_name = row[1]
  		year = row[2]
  		first_name = row[3]
  		last_name = row[4]
  		username = row[5]
  		password = row[6]
  		student = Student.find_by(:username => username)
  		if student.nil?
  			student = Student.new
  			student.money = 0
  			student.points = 0
  			user = User.create!(:username=>username,:password=>password)
  		else
  			user = User.find_by(:username => username)
  		end
  		  	student.class_name = class_name
  			student.class_no = class_number
  			student.year_id = Year.find_by(:name => year).id
  			student.fname = first_name
  			student.lname = last_name
  			student.username = username
  			student.password = password
  			user.password = password
  			user.password_confirmation = password
  			student.user_id = user.id
  			user.save!
  			student.save!
  	end
  	FileUtils.rm tmp.path
  end



end
