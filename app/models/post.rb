class Post < ApplicationRecord
	mount_uploader :image, PostImageUploader
	mount_uploader :file , PostFileUploader
end
