class User < ApplicationRecord
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable,:rememberable, :trackable, :validatable

	def set_admin
		if (Admin.where(:user_id=>self.id).first ==nil)
			Admin.create!(:user_id=>self.id)
		end
	end

	def is_admin
		admin = Admin.where(:user_id=>self.id).first
		if admin == nil
			return false
		else 
			return true
		end
	end

	def get_student
		student = Student.where(:user_id=>self.id).first
		return student
	end
	def email_required?
		false
	end

	def email_changed?
		false
	end
end
