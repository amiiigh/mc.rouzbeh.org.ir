json.extract! badge, :id, :name, :price, :description, :image, :created_at, :updated_at
json.url badge_url(badge, format: :json)