class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	before_action :configure_permitted_parameters, if: :devise_controller?
	def authenticate_admin
		#use Devise's method
		authenticate_user!
		#add your own stuff
		unless current_user.is_admin
			flash[:alert] = 'you are not admin'
			redirect_to root_path
		end
	end


	protected
	def configure_permitted_parameters
		added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
		devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
		devise_parameter_sanitizer.permit :account_update, keys: added_attrs
	end 
end
