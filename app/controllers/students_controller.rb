class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy,:add_badge]
  before_action :authenticate_admin, only: [:edit, :update, :destroy, :create, :new,:import]
  # GET /students
  # GET /students.json
  def index
    @students = Student.where(:class_name => sort_class, :year_id => sort_year).all
  end

  def add_badge
    @badge = Badge.find_by(:id => params[:b_id])
    if @badge.price < @student.money
      Badger.create!(:student_id => params[:id], :badge_id => params[:b_id])
      @student.money -= @badge.price
      @student.save!
    end
    redirect_to :controller => 'students' , :action => 'show'
  end
  
  def import
    Student.import(params[:file])
    redirect_to students_path
  end

  # GET /students/1
  # GET /students/1.json
  def show
    # @badges = @student.badges
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)
    @user = User.create!(:username=>student_params['username'],:password=>student_params['password'])
    @student.user_id = @user.id
    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      @user = User.find(@student.user_id)
      if @student.update(student_params) and
        @user.update(:username=>student_params['username'],:password=>student_params['password'])
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  def sort_year
    @current_year_config = Config.where(:key => 'current_year').first
    if (@current_year_config.nil?)
      @current_year = Year.first
    else
      @current_year = Year.where(:name => @current_year_config.value).first
    end
    if (params[:year])
      params[:year][:id]
    else
      @current_year
    end
  end

  def sort_class
    params[:class_name] || "الف"
  end
  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @user = User.find(@student.user_id)
    @student.destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_student
    @student = Student.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def student_params
    params.require(:student).permit(:fname, :lname, :class_name, :class_no, :points, :year_id,:avatar,:money,:username,:password)
  end
end
