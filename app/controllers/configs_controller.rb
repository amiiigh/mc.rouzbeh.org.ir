class ConfigsController < ApplicationController
  before_action :set_config, only: [:edit, :update, :destroy]
  before_action :authenticate_admin

  def index
    @configs = Config.all
  end

  # GET /lessons/1
  # GET /lessons/1.json
  def show
  end

  # GET /lessons/new
  def new
    @config = Config.new
  end

  # GET /lessons/1/edit
  def edit
  end
  # POST /configs
  # POST /configs.json
  def create
    @config = Config.new(config_params)

    respond_to do |format|
      if @config.save
        format.html { redirect_to configs_path, notice: 'Config was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /configs/1
  # PATCH/PUT /configs/1.json
  def update
    respond_to do |format|
      if @config.update(config_params)
        format.html { redirect_to configs_path, notice: 'Config was successfully updated.' }
      else
        format.html { render configs_path }
      end
    end
  end

  # DELETE /configs/1
  # DELETE /configs/1.json
  def destroy
    @config.destroy
    respond_to do |format|
      format.html { redirect_to configs_url, notice: 'Config was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_config
      @config = Config.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def config_params
      params.require(:config).permit(:key, :value)
    end
end
