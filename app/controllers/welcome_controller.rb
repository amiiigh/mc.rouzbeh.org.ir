class WelcomeController < ApplicationController
	def index
		@posts = Post.order('created_at DESC')
	end

	def setting 
		@config = Config.all
		@current_year = Year.find_by(:id => @config.current_year).name
	end
end
