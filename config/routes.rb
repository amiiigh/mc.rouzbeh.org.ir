Rails.application.routes.draw do
  resources :downloads
  resources :lessons
  resources :configs
  resources :years
  devise_for :users
  resources :posts
  resources :students
  resources :badges
  post 'students/:id/add_badge/:b_id' => 'students#add_badge' ,as: 'add_badge'
  root 'welcome#index'
  post 'students/import' => 'students#import', as: 'import_students'
end
